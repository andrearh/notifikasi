package com.training.microservices.arh.notifikasi.service;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.microservices.arh.notifikasi.dto.NotificationRequest;

@Service
public class NotifikasiKafkaListeners {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotifikasiKafkaListeners.class);

    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(topics = "${kafka.topic.notifikasi.request}")
    public void terimaRequestNotifikasi(String message){
        LOGGER.debug("Terima message request notifikasi [{}]", message);

        try {
            NotificationRequest req = objectMapper.readValue(message, NotificationRequest.class);
            LOGGER.debug("Konversi json menjadi object : [{}]", req);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

    }
}
